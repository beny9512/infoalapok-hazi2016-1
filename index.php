<?php $FILENAME = "index.php"; ?>
<html>
<head>
  <title>Informatika alapok házi generátor</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="icon" type="image/png" href="images/favicon.png">
  <style type="text/css">
  a, a:active, a:visited {
    text-decoration: none !important;
    color: black !important;
  }
  a:hover {
    text-decoration: underline !important;
    color: black !important;
  }
  #underline {
    border-bottom: 1px dashed #ccc;
    width: auto;
    margin-top: -20px;
    margin-bottom: 10px;
  }
  </style>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>
<body>
  <div class="container">
    <?php if (!isset($_POST["submit"])) { ?>
      <br>
      <h2><a href="<?php echo $FILENAME; ?>">
        Informatika alapok házi generátor</a></h2>
        <div class="row">
          <div class="two columns">Adatok megadása</div>
          <div class="ten columns">
            <?php
            if (isset($_GET["error"]) && $_GET["error"] == 1) {
              echo "<code style=\"color: red;\">Hibás adatokat adtál meg! Próbáld újra!</code>";
            }
            ?>
            <form method="post" action="<?php echo $FILENAME; ?>">
              <table class="u-full-width">
                <tbody>
                  <tr>
                    <td><label for="VNev">Vezetéknév</label>
                      <input class="u-full-width" type="text" placeholder="A" id="VNev" name="VNev" autocomplete="off"></td>
                      <td><label for="KNev">Keresztnév</label>
                        <input class="u-full-width" type="text" placeholder="B" id="KNev" name="KNev" autocomplete="off"></td>
                      </tr>
                      <tr>
                        <td colspan="2"><input class="button-primary" name="submit" type="submit" value="Mehet!"></td>
                      </tr>
                    </tbody>
                  </table>
                </form>
              </div>
            </div>
            <?php
          } else {
            $vnev = $_POST["VNev"];
            $knev = $_POST["KNev"];
            $szamok_tomb = array(
              1 => 'A', 2 => 'Á', 3 => 'B', 4 => 'C', 5 => 'Cs', 6 => 'D',
              7 => 'Dz', 8 => 'Dzs', 9 => 'E', 10 => 'É', 11 => 'F', 12 => 'G',
              13 => 'Gy', 14 => 'H', 15 => 'I', 16 => 'Í', 17 => 'J', 18 => 'K',
              19 => 'L', 20 => 'Ly', 21 => 'M', 22 => 'N', 23 => 'Ny', 24 => 'O',
              25 => 'Ó', 26 => 'Ö', 27 => 'Ő', 28 => 'P', 29 => 'Q', 30 => 'R',
              31 => 'S', 32 => 'Sz', 33 => 'T', 34 => 'Ty', 35 => 'U', 36 => 'Ú',
              37 => 'Ü', 38 => 'Ű', 39 => 'V', 40 => 'W', 41 => 'X', 42 => 'Y',
              43 => 'Z', 44 => 'Zs'
            );
            $kapuk_tomb = array(
              1 => 'AND', 2 => 'OR', 3 => 'XOR', 4 => 'NAND', 5 => 'NOR', 6 => 'AND',
              7 => 'OR', 8 => 'XOR', 9 => 'NAND', 10 => 'NOR', 11 => 'AND', 12 => 'OR',
              13 => 'XOR', 14 => 'NAND', 15 => 'NOR', 16 => 'AND', 17 => 'OR', 18 => 'XOR',
              19 => 'NAND', 20 => 'NOR', 21 => 'AND', 22 => 'OR', 23 => 'XOR', 24 => 'NAND',
              25 => 'NOR', 26 => 'AND', 27 => 'OR', 28 => 'XOR', 29 => 'NAND', 30 => 'NOR',
              31 => 'AND', 32 => 'OR', 33 => 'XOR', 34 => 'NAND', 35 => 'NOR', 36 => 'AND',
              37 => 'OR', 38 => 'XOR', 39 => 'NAND', 40 => 'NOR', 41 => 'AND', 42 => 'OR',
              43 => 'XOR', 44 => 'NAND'
            );
            if (is_numeric($vnev) || is_numeric($knev) ||
            array_search(ucfirst($vnev), $szamok_tomb) == FALSE ||
            array_search(ucfirst($knev), $szamok_tomb) == FALSE) {
              header("Location: $FILENAME?error=1");
            }
            $vnev_sz = array_search(ucfirst($vnev), $szamok_tomb);
            $knev_sz = array_search(ucfirst($knev), $szamok_tomb);
            $vnev_lg = $kapuk_tomb[$vnev_sz];
            $knev_lg = $kapuk_tomb[$knev_sz];
            ?>
            <div class="row">
              <br>
              <div class="two columns">Adatok</div>
              <div class="seven columns">
                <h5>Vezetéknév értéke: <?php echo ucfirst($vnev) . " - " . $vnev_sz . " - " . $vnev_lg; ?></h5>
                <h5>Keresztnév értéke: <?php echo ucfirst($knev) . " - " . $knev_sz . " - " . $knev_lg; ?></h5>
              </div>
              <div class="two columns">
                <a class="button" href="<?php echo $FILENAME; ?>">Újra</a>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="one column">1. feladat</div>
              <div class="three columns">
                <?php echo "<h6>" . ucfirst($vnev) . " bináris alakban: " . decbin($vnev_sz) . "</h6>";  ?>
                <?php echo "<h6>" . ucfirst($knev) . " bináris alakban: " . decbin($knev_sz) . "</h6>";  ?>
                <div id="underline">&nbsp;</div>
                <?php echo "<h6>" . ucfirst($vnev) . " oktális alakban: " . decoct($vnev_sz) . "</h6>";  ?>
                <?php echo "<h6>" . ucfirst($knev) . " oktális alakban: " . decoct($knev_sz) . "</h6>";  ?>
                <div id="underline">&nbsp;</div>
                <?php echo "<h6>" . ucfirst($vnev) . " hexadecimális alakban: " . dechex($vnev_sz) . "</h6>";  ?>
                <?php echo "<h6>" . ucfirst($knev) . " hexadecmiális alakban: " . dechex($knev_sz) . "</h6>";  ?>
              </div>
              <div class="one column">2. feladat</div>
              <div class="three columns">
                <?php
                           $vnev_split = str_split($vnev_sz, 1);
                           echo "A ".$vnev_sz." tömörített BCD alakban:</br>";
                           foreach ($vnev_split as $x) {
                               echo sprintf('%04d', decbin($x))." ";
                           }
                           echo "</br>";
                           echo "A ".$vnev_sz." zónázott BCD alakban:</br>";
                           foreach ($vnev_split as $x) {
                               echo sprintf('%08d', decbin($x))." ";
                           }
                           echo "</br>";
                           echo "</br>";
                           $knev_split = str_split($knev_sz, 1);
                           echo "A ".$knev_sz." tömörített BCD alakban:</br>";
                           foreach ($knev_split as $x) {
                               echo sprintf('%04d', decbin($x))." ";
                           }
                           echo "</br>";
                           echo "A ".$knev_sz." zónázott BCD alakban:</br>";
                           foreach ($knev_split as $x) {
                               echo sprintf('%08d', decbin($x))." ";
                           }
                ?>
              </div>
              <div class="one column">3. feladat</div>
              <div class="three columns">
                <?php echo "<h6>".$vnev_sz.",". $knev_sz . " átváltása binárisba:</h6>";  ?>
                <?php
                  echo decbin($vnev_sz) . ",";
                  $knev_divided = $knev_sz/100;
                  $f3 = "";
                  for($i = 1; $i <= 10; $i++){
                      if($knev_divided * 2 > 1){
                          echo "1";
                          $f3 .= "1";
                          $knev_divided = $knev_divided * 2 - 1;
                      }else{
                          echo "0";
                          $f3 .= "0";
                          $knev_divided = $knev_divided * 2;
                      }
                  }
                  $f3_veg = decbin($vnev_sz) . "," . $f3;
                  ?>
              </div>
            </div>
            <div class="row">
              <div class="one column">4. feladat</div>
              <div class="three columns">
                <?php echo "<h6>A +" . $vnev_sz . " egyes komplemens kódban 8 biten: 00" . decbin($vnev_sz) . "</h6>";  ?>
                <?php echo "<h6>Kettes komplemens kódban: 00" . decbin($knev_sz) . " (mivel nagyobb, mint 0)</h6>";  ?>
              </div>
              <div class="one column">5. feladat</div>
              <div class="three columns">
                <?php echo "<h6>A " . $knev_sz . " bináris alakja 8 biten: 00" . decbin($knev_sz) ."</h6>";  ?>
                <?php $temp1 = str_replace("1", "a", "00" . decbin($knev_sz));
                $temp2 = str_replace("0", "1", $temp1);
                $temp3 = str_replace("a", "0", $temp2); ?>
                <?php echo "<h6>A -" . $knev_sz . " bináris alakja 8 biten: " . $temp3 ."</h6>";  ?>
              </div>
              <div class="one column">6. feladat</div>
              <div class="three columns">
                <p>A <?php echo $vnev_sz . "," . $knev_sz; ?> lebegőpontos alakban:<br>
                  A 3. feladat alapján a <?php echo $vnev_sz . "," . $knev_sz; ?> bináris alakja:
                  <?php echo $f3_veg; ?></p>
                  <p>Eltolás: <?php echo $f3_veg; ?> * 10^5 = <?php echo $f3_veg * pow(10, 5); ?></p>
              </div>
            </div>
            <div class="row">
              <div class="one column">7. feladat</div>
              <div class="eleven columns">
                <img src="logic_gate_img.php?arg1=<?php echo $vnev_lg; ?>&arg2=<?php echo $knev_lg; ?>">
              </div>
            </div>
            <?php
          }
          ?>
        </div>
      </body>
      </html>
