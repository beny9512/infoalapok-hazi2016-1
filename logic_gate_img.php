<?php
header('Content-type: image/png');
$png_image = imagecreatefrompng('images/logic_gate.png');

$black = imagecolorallocate($png_image, 0, 0, 0);
putenv('GDFONTPATH=' . realpath('.'));
$font_path = 'tnr.ttf';
if (!empty($_GET["arg1"]) && !empty($_GET["arg2"])) {
  $text1 = $_GET["arg1"];
  $text2 = $_GET["arg2"];
} else {
  $text1 = NULL;
  $text2 = NULL;
}
imagettftext($png_image, 9, 0, 86, 55, $black, $font_path, $text1); // vezetéknév
imagettftext($png_image, 9, 0, 86, 125, $black, $font_path, $text2); // keresztnév
if ($text1 == "OR") { // A
  $text3 = 0;
} elseif ($text1 == "AND") {
  $text3 = 0;
} elseif ($text1 == "XOR") {
  $text3 = 0;
} elseif ($text1 == "NAND") {
  $text3 = 1;
} elseif ($text1 == "NOR") {
  $text3 = 1;
}
if ($text2 == "OR") { // B
  $text4 = 1;
} elseif ($text2 == "AND") {
  $text4 = 0;
} elseif ($text2 == "XOR") {
  $text4 = 1;
} elseif ($text2 == "NAND") {
  $text4 = 1;
} elseif ($text2 == "NOR") {
  $text4 = 0;
}
if ($text3 == "0" && $text4 == "0") { // NOR
  $text5 = 1;
} else {
  $text5 = 0;
}
if ($text3 == "1" && $text4 == "1") { // NAND
  $text6 = 0;
} else {
  $text6 = 1;
}
if ($text5 == "0" && $text6 == "0") { // OR
  $text7 = 0;
} else {
  $text7 = 1;
}
if ($text3 == "1") { // XOR
  $text8 = 0;
} else {
  $text8 = 1;
}
imagettftext($png_image, 9, 0, 130, 42, $black, $font_path, $text3); // OR
imagettftext($png_image, 9, 0, 180, 65, $black, $font_path, $text3); // OR
imagettftext($png_image, 9, 0, 305, 42, $black, $font_path, $text3); // OR
imagettftext($png_image, 9, 0, 180, 110, $black, $font_path, $text4); // XOR
imagettftext($png_image, 9, 0, 130, 140, $black, $font_path, $text4); // XOR
imagettftext($png_image, 9, 0, 305, 125, $black, $font_path, $text5); // NOR
imagettftext($png_image, 9, 0, 210, 152, $black, $font_path, $text5); // NOR
imagettftext($png_image, 9, 0, 285, 80, $black, $font_path, $text6); // NAND
imagettftext($png_image, 9, 0, 390, 18, $black, $font_path, $text7); // X
imagettftext($png_image, 9, 0, 390, 90, $black, $font_path, $text8); // Y
imagepng($png_image);
imagedestroy($png_image);
?>
